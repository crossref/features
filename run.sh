#!/usr/bin/env bash

# sed behaves differently on MacOS and Linux. 
# https://stackoverflow.com/questions/43171648/sed-gives-sed-cant-read-no-such-file-or-directory
SEDOPTION="-i"
if [[ "$OSTYPE" == "darwin"* ]]; then
  SEDOPTION="-i ''"
fi
export SEDOPTION

# Convert gherkin .feature to markdown using some simple text substitutions. 
function convertToMd {
  filePath=$1
  echo "Convert $filePath to markdown ... "
  ls "$filePath"

  sed -E $SEDOPTION "s/^Feature: *(.*)$/---\ntitle: \"\1\" \n---/g" "$filePath"
  
  # Remove leading space or markdown thinks it's a code block.
  sed -E $SEDOPTION "s/^ *//g" "$filePath"

  # Angle brackets are used for variables. Escape so they show up.
  sed -E $SEDOPTION "s/>/\\\>/g" "$filePath"
  sed -E $SEDOPTION "s/</\\\</g" "$filePath"

  # H2 level 
  sed -E $SEDOPTION "s/(Rule): *(.*)$/## \1: \2 \n/g" "$filePath"
  
  # H3 level
  sed -E $SEDOPTION "s/(Background|Scenario|Scenario Outline): *(.*)$/### \1: \2 \n/g" "$filePath"

  # Asterisk used for 'and'
  sed -E $SEDOPTION "s/^\* *(.*)$/- ** * ** \1 /g" "$filePath"

  # Reserved words in scenarios rendered as lists.
  sed -E $SEDOPTION "s/^(Given|When|Then|And|But) *(.*)$/- **\1** \2 /g" "$filePath"

  # Examples tables - just indent
  sed -E $SEDOPTION "s/^(Examples:|\|) *(.*)$/      \1 \2 /g" "$filePath"
}

# Enable this to be used later in the find -exec.
export -f convertToMd

# Fetch all submodules, including the projects and the hugo 'docsy' template.
git submodule update --init
git submodule update --init --recursive themes/docsy

npm install
(cd themes/docsy && npm install)

# The 'content' directory is the Hugo content root. 
# The 'content/docs' gets populated by this script, and is .gitignored .
rm -r content/docs
mkdir -p content/docs
echo -e "---\ntitle: Features\n---\nBrowse our library of features in the menu." > "content/docs/_index.md"

# Each of these is a git submodule corresponding to a different git project.
for proj in modules/*/ ; do
    if [ $proj != "target/" ]; then
      export projName=$(basename $proj)
      echo "Looking at $projName in $proj "
      export projDir="content/docs/$projName"

      # Get this module's origin URL. Convert git@gitlab.com:crossref/xyz.git to a URL.
      repo=$(cd $proj && git config --get remote.origin.url)
      repo="${repo/git@gitlab.com:/https:\/\/gitlab.com\/}"
      repo="${repo/.git//}"

      # Create the directory for this project in the content/docs.
      mkdir -p "$projDir"
      echo -e "---\ntitle: $projName\n---\nRepository: [$repo]($repo) \n\n" > "$projDir/_index.md"

      # Copy .feature files and then convert them from Gherkin to Markdown.
      find $proj -name "*.feature" -exec bash -c 'export x={};export fname="$(basename $x)";cp $x "$projDir/${fname/.feature/.md}"' \;
      find $projDir -name "*.md" -exec bash -c 'convertToMd "$0"' {} \;
    fi 
done

hugo

