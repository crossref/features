# Crossref Features

**Visit <https://crossref.gitlab.io/features/> to browse**

This pulls in Feature files from our greenfield software projects and renders them into one browseable library. 

## Misc

- CI runs on a schedule and pulls in the latest `main` for each project.  
- Each project is a submodule. 
- Feature files from all projects are collected into one place.
- The JavaScript Cucumber runner is used. It doesn't execute tests, just formats them into one page.

To list projects:
```
git submodule status
```

If you start a new codebase, add it as a git submodule.
